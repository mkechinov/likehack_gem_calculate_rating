require 'spec_helper'

describe CalculateRating do

	it 'calculate all shares for provided link' do
		url = "http://www.telegraph.co.uk/finance/personalfinance/consumertips/10727637/My-16-year-fight-over-PC-World-laptop-loan.html"
		data = CalculateRating.calculate(url)
		expect(data[:fb_reposts]).not_to eq nil
		expect(data[:tw_reposts]).not_to eq nil
		expect(data[:in_reposts]).not_to eq nil
		expect(data[:gp_reposts]).not_to eq nil
		p data[:fb_reposts]
		p data[:tw_reposts]
		p data[:in_reposts]
		p data[:gp_reposts]
		expect(data[:error]).to eq nil
	end

	it 'calculate only fb shares' do
		url = "http://www.google.ru"
		p CalculateRating.get_facebook_reposts(url)
	end

	it 'calculate only tw shares' do
		url = "http://www.google.ru"
		p CalculateRating.get_twitter_reposts(url)
	end

	it 'calculate only in shares' do
		url = "http://www.google.ru"
		p CalculateRating.get_linkedin_reposts(url)
	end

	it 'calculate only g+ shares' do
		url = "http://www.google.ru"
		p CalculateRating.get_google_plus_reposts(url)
	end

	it 'when no link provided' do
		url = ""
		data = CalculateRating.calculate(url)
		expect(data[:error]).not_to eq nil
	end

end