# CalculateRating

It gets amount of shares of url from social networks

## Installation

Add this line to your application's Gemfile:

    gem "calculate_rating"

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install calculate_rating

## Usage

Just choose url you want to calculate reposts and pass it into calculate method:

	your_url = "http://google.com"
	CalculateRating.calculate(your_url)

After calculate operation is finished, you will receive hashes like those below.

On success:

	{:fb_reposts=>25170, :tw_reposts=>25170, :in_reposts=>25170, :gp_reposts=>25170, :error=>nil}

* As you can see, it contains separate numbers of shares for each API's and error message that is nil.

On failure:

	{:fb_reposts=>0, :tw_reposts=>0, :in_reposts=>0, :gp_reposts=>0, :error=>"out. we couldn't get shares for some reason"}

* The amounts of shares are 0, and an error message inside.