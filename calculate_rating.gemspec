# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'calculate_rating/version'

Gem::Specification.new do |spec|
  spec.name          = "calculate_rating"
  spec.version       = CalculateRating::VERSION
  spec.authors       = ["Katya Barasheva"]
  spec.email         = ["ekaterina.barasheva@mkechinov.ru"]
  spec.summary       = %q{It gets amount of total shares of url from social networks.}
  spec.description   = %q{It gets amount from facebook, twitter, linkedin and googleplus total shares of url. }
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

	spec.add_dependency "rspec"
  spec.add_dependency "bundler", "~> 1.2"
  spec.add_dependency "rake"
end