require "calculate_rating/version"
require "open-uri"
require "uri"
require "net/http"
require "net/https"
require 'json'

module CalculateRating

	def self.calculate(url)

		data = { :fb_reposts => 0, :tw_reposts => 0, :in_reposts => 0, :gp_reposts => 0, :error => nil }

		if url != nil && url != ""

			begin
				data[:fb_reposts] = get_facebook_reposts(url)
				data[:tw_reposts] = get_twitter_reposts(url)
				data[:in_reposts] = get_linkedin_reposts(url)
				data[:gp_reposts] = get_google_plus_reposts(url)

				p "Finish, result: #{data}"
				return data # Successful final

			rescue
				data[:error] = "out. we couldn't get shares for some reason"
			end

		else
			data[:error] = "out. no url provided"
			p data[:error]
			return data	# Out
		end
	end

	# Gets FB shares for provided link
	# @param [string] url Link to feed
	# @return integer | 0

	def self.get_facebook_reposts(url)
		begin
			request_url = 'http://graph.facebook.com/?id='+url
			html = open(URI.escape(request_url)).read
			return JSON.parse(html)['shares']
		rescue
			return 0
		end
	end

	# Gets TW shares for provided link
	# @param [string] url Link to feed
	# @return integer | 0
	def self.get_twitter_reposts(url)
		begin
			request_url = 'http://urls.api.twitter.com/1/urls/count.json?url='+url
			html = open(URI.escape(request_url)).read
			return JSON.parse(html)['count']
		rescue
			return 0
		end
	end

	# Gets Linkedin shares for provided link
	# @param [string] url Link to feed
	# @return integer | 0

	def self.get_linkedin_reposts(url)
		begin
			request_url = 'http://www.linkedin.com/countserv/count/share?url='+url+'&format=json'
			html = open(URI.escape(request_url)).read
			return JSON.parse(html)['count']
		rescue
			return 0
		end
	end

	# Gets G+ shares for provided link
	# @param [string] url Link to feed
	# @return integer | 0
	# AIzaSyA7DPJgKhVYIqiBt2TpD0uOkxIjLXAnXOo

	def self.get_google_plus_reposts(url)
		begin
			# @toSend = {"method" => "pos.plusones.get", "id" => "p", "params" => { "nolog" => true, "id" => url, "source" => "widget", "userId" => "@viewer", "groupId" => "@self"}, "jsonrpc" => "2.0", "key" => "p", "apiVersion" => "v1"}.to_json
			@toSend = {"method" => "pos.plusones.get", "id" => "p", "params" => { "nolog" => true, "id" => url, "source" => "widget", "userId" => "@viewer", "groupId" => "@self"}, "jsonrpc" => "2.0", "key" => "p", "apiVersion" => "v1"}.to_json
			uri = URI.parse('https://clients6.google.com/rpc?key=AIzaSyA7DPJgKhVYIqiBt2TpD0uOkxIjLXAnXOo')
			https = Net::HTTP.new(uri.host,uri.port)
			https.use_ssl = true
			req = Net::HTTP::Post.new(uri.path, initheader = {'Content-Type' =>'application/json'})
			req['foo'] = 'bar'
			req.body = "[ #{@toSend} ]"
			res = https.request(req)
			item = JSON.parse(res.body)
			if item && item[0] && item[0] && item[0]['result'] && item[0]['result']['metadata'] && item[0]['result']['metadata']['globalCounts'] && item[0]['result']['metadata']['globalCounts']['count']
				count = item[0]['result']['metadata']['globalCounts']['count']
			end
			return count.to_i
		rescue
			return 0
		end
	end
end